var player1 = document.getElementById('player1-1'); //init position
var player2 = document.getElementById('player2-1');
var ghost = document.getElementById('ghost');
var right_box = document.getElementById('right-box');
let rect = right_box.getBoundingClientRect();
var x1 = rect['left'] + 900 + "px";
var y1 = rect['top'] + 300 + "px";
var x2 = rect['left'] + 250 + "px";
var y2 = rect['top'] + 300 + "px";
var x3 = rect['left'] + 550 + "px";
var y3 = rect['top'] + 650 + "px";
player1.style.left = x1;
player1.style.top = y1;
player2.style.left = x2;
player2.style.top = y2;
ghost.style.left = x3;
ghost.style.top = y3;

function gamerule() { //total game rule
    var time = document.getElementById('Check_i');
    var count1 = document.getElementById('count1');
    var count2 = document.getElementById('count2');
    var safeplace1 = document.getElementById('safeplace1');
    var safeplace2 = document.getElementById('safeplace2');
    var safeplace3 = document.getElementById('safeplace3');
    var safeplace4 = document.getElementById('safeplace4');
    var outcome = document.getElementById('outcome');
    var winner = document.getElementById('winner');
    var loser = document.getElementById('loser');
    var timeLeft = document.getElementById('timeLeft');
    let rect_safeplace1 = safeplace1.getBoundingClientRect();
    let rect_safeplace2 = safeplace2.getBoundingClientRect();
    let rect_safeplace3 = safeplace3.getBoundingClientRect();
    let rect_safeplace4 = safeplace4.getBoundingClientRect();
    player1.style.opacity = 1;
    player2.style.opacity = 1;
    var delta_x1 = 0;
    var delta_y1 = 0;
    var delta_x2 = 0;
    var delta_y2 = 0;
    var delta_xghost = 0;
    var delta_yghost = 0;
    // for (var key in rect) { //check the axis of the game place
    //     if(typeof rect[key] !== 'function') {
    //         console.log(`${ key } : ${ rect[key] }`);
    //     }
    // }
    // for (var key in rect_safeplace1) { //check the axis of the safe place
    //     if (typeof rect_safeplace1[key] !== 'function') {
    //         console.log(`${ key } : ${ rect_safeplace1[key] }`);
    //     }
    // }

    function delay(n) {
        return new Promise(function (resolve) {
            setTimeout(resolve, n * 1000);
        });
    }
    window.addEventListener('keydown', function (e) { //move funciton(keydown)
        var keyID = e.key;
        switch (keyID) { //player1
            case 'ArrowUp':
                delta_y1 = 1;
                break;
            case 'ArrowDown':
                delta_y1 = -1;
                break;
            case 'ArrowRight':
                delta_x1 = -1;
                break;
            case 'ArrowLeft':
                delta_x1 = 1;
                break;

            case 'u' || 'U': //player2
                delta_y2 = 1;
                break;
            case 'j' || 'J':
                delta_y2 = -1;
                break;
            case 'k' || 'K':
                delta_x2 = -1;
                break;
            case 'h' || 'H':
                delta_x2 = 1;
                break;

            case 'w' || 'W': //ghost
                delta_yghost = 1;
                break;
            case 's' || 'S':
                delta_yghost = -1;
                break;
            case 'd' || 'D':
                delta_xghost = -1;
                break;
            case 'a' || 'A':
                delta_xghost = 1;
                break;
        }
    }, false)

    window.addEventListener('keyup', function (e) { //move funciton(keyup)
        var keyID = e.key;
        switch (keyID) { //player1
            case 'ArrowUp':
                delta_y1 = 0;
                break;
            case 'ArrowDown':
                delta_y1 = 0;
                break;
            case 'ArrowRight':
                delta_x1 = 0;
                break;
            case 'ArrowLeft':
                delta_x1 = 0;
                break;

            case 'u' || 'U': //player2
                delta_y2 = 0;
                break;
            case 'j' || 'J':
                delta_y2 = 0;
                break;
            case 'k' || 'K':
                delta_x2 = 0;
                break;
            case 'h' || 'H':
                delta_x2 = 0;
                break;

            case 'w' || 'W': //ghost
                delta_yghost = 0;
                break;
            case 's' || 'S':
                delta_yghost = 0;
                break;
            case 'd' || 'D':
                delta_xghost = 0;
                break;
            case 'a' || 'A':
                delta_xghost = 0;
                break;

        }
    }, false)

    setInterval(function () {
        //border rules and move rules
        var unit = 10;
        if (parseInt(x1.slice(0, -2)) < rect['left'] - 2) { //player1 border limit
            x1 = rect['left'] + "px";
        }
        if (parseInt(x1.slice(0, -2)) > rect['right'] - 100) {
            x1 = rect['right'] - 101 + "px";
        }
        if (parseInt(y1.slice(0, -2)) < rect['top'] - 13) {
            y1 = rect['top'] - 13 + "px";
        }
        if (parseInt(y1.slice(0, -2)) > rect['bottom'] - 90) {
            y1 = rect['bottom'] - 91 + "px";
        }
        y1 = parseInt(y1.slice(0, -2)) - delta_y1 * unit + "px"; //player1 move rules
        x1 = parseInt(x1.slice(0, -2)) - delta_x1 * unit + "px";
        player1.style.top = y1;
        player1.style.left = x1;

        if (parseInt(x2.slice(0, -2)) < rect['left'] - 2) { //player2 border limit
            x2 = rect['left'] + "px";
        }
        if (parseInt(x2.slice(0, -2)) > rect['right'] - 100) {
            x2 = rect['right'] - 101 + "px";
        }
        if (parseInt(y2.slice(0, -2)) < rect['top'] - 13) {
            y2 = rect['top'] - 13 + "px";
        }
        if (parseInt(y2.slice(0, -2)) > rect['bottom'] - 90) {
            y2 = rect['bottom'] - 91 + "px";
        }

        y2 = parseInt(y2.slice(0, -2)) - delta_y2 * unit + "px";//player2 move rules
        x2 = parseInt(x2.slice(0, -2)) - delta_x2 * unit + "px";
        player2.style.top = y2;
        player2.style.left = x2;

        if (parseInt(x3.slice(0, -2)) < rect['left'] - 1) { //ghost border limit
            x3 = rect['left'] + "px";
        }
        if (parseInt(x3.slice(0, -2)) > rect['right'] - 100) {
            x3 = rect['right'] - 101 + "px";
        }
        if (parseInt(y3.slice(0, -2)) < rect['top'] - 13) {
            y3 = rect['top'] - 13 + "px";
        }
        if (parseInt(y3.slice(0, -2)) > rect['bottom'] - 90) {
            y3 = rect['bottom'] - 90 + "px";
        }

        if (parseInt(y3.slice(0, -2)) < rect_safeplace1['bottom'] - 20 && parseInt(x3.slice(0, -2)) < rect_safeplace1['right'] - 10) { //ghost safe place limit
            y3 = parseInt(y3.slice(0, -2)) + 2 * 1 * unit + "px";
            x3 = parseInt(x3.slice(0, -2)) + 2 * 1 * unit + "px";
        } else if (parseInt(y3.slice(0, -2)) < rect_safeplace2['bottom'] - 20 && parseInt(x3.slice(0, -2)) > rect_safeplace2['left'] - 60) {
            y3 = parseInt(y3.slice(0, -2)) + 2 * 1 * unit + "px";
            x3 = parseInt(x3.slice(0, -2)) - 2 * 1 * unit + "px";
        } else if (parseInt(y3.slice(0, -2)) > rect_safeplace3['top'] - 80 && parseInt(x3.slice(0, -2)) < rect_safeplace3['right'] - 10) {
            y3 = parseInt(y3.slice(0, -2)) - 2 * 1 * unit + "px";
            x3 = parseInt(x3.slice(0, -2)) + 2 * 1 * unit + "px";
        } else if (parseInt(y3.slice(0, -2)) > rect_safeplace4['top'] - 80 && parseInt(x3.slice(0, -2)) > rect_safeplace4['left'] - 60) {
            y3 = parseInt(y3.slice(0, -2)) - 2 * 1 * unit + "px";
            x3 = parseInt(x3.slice(0, -2)) - 2 * 1 * unit + "px";
        }
        y3 = parseInt(y3.slice(0, -2)) - delta_yghost * unit + "px";//ghost move rules
        x3 = parseInt(x3.slice(0, -2)) - delta_xghost * unit + "px";
        ghost.style.top = y3;
        ghost.style.left = x3;

        //player disapper rules
        if (Math.abs(parseInt(x3.slice(0, -2)) - parseInt(x1.slice(0, -2))) < 30 && Math.abs(parseInt(y3.slice(0, -2)) - parseInt(y1.slice(0, -2))) < 40) { //ghost get player1
            player1.style.opacity = 0;
        }else if(parseInt(count1.innerText)>=10){ //over time in safe place
            player1.style.opacity = 0;
        }
        if (Math.abs(parseInt(x3.slice(0, -2)) - parseInt(x2.slice(0, -2))) < 30 && Math.abs(parseInt(y3.slice(0, -2)) - parseInt(y2.slice(0, -2))) < 40) { //ghost get player2
            player2.style.opacity = 0;
        }else if(parseInt(count2.innerText)>=10){ //over time in safe place
            player2.style.opacity = 0;
        }

        //game over rules
        if (parseInt(time.innerText.slice(0, -1)) === 0) { //game over condition1:time out
            right_box.style.background = 'red';
            player1.style.display = 'none';
            player2.style.display = 'none';
            ghost.style.display = 'none';
            outcome.style.zIndex = 1;
            winner.innerText = 'People';// game over panel
            loser.innerText = 'Ghost';
            timeLeft.innerText = document.getElementById("Check_i").value;
        }

        if (player1.style.opacity == 0 && player2.style.opacity == 0) { //game over condition2:ghost get all player
            pausegame();// if game over and ghost win need to pause timer
            right_box.style.background = 'red';
            player1.style.display = 'none';
            player2.style.display = 'none';
            ghost.style.display = 'none';
            outcome.style.zIndex = 1;
            winner.innerText = 'Ghost';// game over panel
            loser.innerText = 'People';
            timeLeft.innerText = document.getElementById("Check_i").value;
        }

    }, 50)

    setInterval(function () { //cal the value of count
        if (parseInt(y1.slice(0, -2)) < rect_safeplace1['bottom'] - 20 && parseInt(x1.slice(0, -2)) < rect_safeplace1['right'] - 10) { //player1 safe place count
            if (count1.innerText<10){
                parseInt(count1.innerText++);
            }
        } else if (parseInt(y1.slice(0, -2)) < rect_safeplace2['bottom'] - 20 && parseInt(x1.slice(0, -2)) > rect_safeplace2['left'] - 60) {
            if (count1.innerText<10){
                parseInt(count1.innerText++);
            }
        } else if (parseInt(y1.slice(0, -2)) > rect_safeplace3['top'] - 80 && parseInt(x1.slice(0, -2)) < rect_safeplace3['right'] - 10) {
            if (count1.innerText<10){
                parseInt(count1.innerText++);
            }
        } else if (parseInt(y1.slice(0, -2)) > rect_safeplace4['top'] - 80 && parseInt(x1.slice(0, -2)) > rect_safeplace4['left'] - 60) {
            if (count1.innerText<10){
                parseInt(count1.innerText++);
            }
        }

        if (parseInt(y2.slice(0, -2)) < rect_safeplace1['bottom'] - 20 && parseInt(x2.slice(0, -2)) < rect_safeplace1['right'] - 10) { //player2 safe place count
            if (count2.innerText<10){
                parseInt(count2.innerText++);
            }
        } else if (parseInt(y2.slice(0, -2)) < rect_safeplace2['bottom'] - 20 && parseInt(x2.slice(0, -2)) > rect_safeplace2['left'] - 60) {
            if (count2.innerText<10){
                parseInt(count2.innerText++);
            }
        } else if (parseInt(y2.slice(0, -2)) > rect_safeplace3['top'] - 80 && parseInt(x2.slice(0, -2)) < rect_safeplace3['right'] - 10) {
            if (count2.innerText<10){
                parseInt(count2.innerText++);
            }
        } else if (parseInt(y2.slice(0, -2)) > rect_safeplace4['top'] - 80 && parseInt(x2.slice(0, -2)) > rect_safeplace4['left'] - 60) {
            if (count2.innerText<10){
                parseInt(count2.innerText++);
            }
        }

    },1000)
}
let SetSecond = 60;
let timeout;
let timer_on = 0;

document.getElementById("start").addEventListener("click", function(){ //start game
    startgame();
    gamerule();
});
document.getElementById("pause").addEventListener("click", pausegame); //pause game

function timedCount() {
    if (SetSecond >= 0) {
        console.log(SetSecond);
        document.getElementById("Check_i").value = SetSecond;
        Check_i.innerHTML = SetSecond + "sec";
        SetSecond--;
        timeout = setTimeout(timedCount, 1000);
    }
}

function startgame() {
    if (!timer_on && SetSecond > 0) {
        document.getElementById("start").removeEventListener("click", startgame);
        timer_on = 1;
        timedCount();
    }
}

function pausegame() {
    if (SetSecond > 0) {
        document.getElementById("start").addEventListener("click", startgame);
        clearTimeout(timeout);
        timer_on = 0;
    }

}

var player1name = document.getElementById("player1name");
var player2name = document.getElementById("player2name");
var player3name = document.getElementById("player3name");
var btn = document.getElementById("namebutton");
var form = document.getElementById("enter");

btn.addEventListener("click", function () { //name box
    document.getElementById("player1").innerHTML = player1name.value;
    document.getElementById("player2").innerHTML = player2name.value;
    document.getElementById("player3").innerHTML = player3name.value;
    var ghost = Math.floor((Math.random(0, 3) * 10) % 3);
    if (ghost == 0) {
        var tmp = document.getElementById("player1").innerHTML;
        document.getElementById("player1").innerHTML = player3name.value + "--上下左右";
        document.getElementById("player2").innerHTML = player2name.value + "--UHJK";
        document.getElementById("player3").innerHTML = tmp + "--WASD" + "(ghost)";
    } else if (ghost == 1) {
        var tmp = document.getElementById("player2").innerHTML;
        document.getElementById("player1").innerHTML = player1name.value + "--上下左右";
        document.getElementById("player2").innerHTML = player3name.value + "--UHJK";
        document.getElementById("player3").innerHTML = tmp  + "--WASD" + "(ghost)";
    } else if (ghost == 2) {
        document.getElementById("player1").innerHTML = player1name.value + "--上下左右";
        document.getElementById("player2").innerHTML = player2name.value + "--UHJK";
        document.getElementById("player3").innerHTML = player3name.value + "--WASD" + "(ghost)";
    }
    form.style.display = "none";
})